# APT Ansible Role

This is an [Ansible][ansible] role for managing [APT][apt] resources.

It is meant to facilitate using the various Ansible apt modules like
[`apt_key`][apt_key] and [`apt_repository`][apt_repository], and adjust
settings in `/etc/apt/`. It does not obsolete individual
[`apt` module][apt_module] invocations themselves.

[ansible]: https://docs.ansible.com/ansible/latest/
[apt]: https://wiki.debian.org/Apt
[apt_key]: https://docs.ansible.com/ansible/latest/modules/apt_key_module.html
[apt_module]: https://docs.ansible.com/ansible/latest/modules/apt_module.html
[apt_repository]: https://docs.ansible.com/ansible/latest/modules/apt_repository_module.html
